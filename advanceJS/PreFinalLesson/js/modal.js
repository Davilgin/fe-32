class Modal {
  openModal() {
    this.modalElement = document.createElement('div')
    this.modalElement.classList.add('modal', 'show')
    this.modalElement.setAttribute('id', 'modal');
    this.modalElement.setAttribute('tabindex', '-1')
    this.modalElement.innerHTML = `
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h5 class="modal-title">Do you want to delete?</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                          </div>
                          <div class="loader-container js-loader-container">
                            <div class="d-flex justify-content-center loader-back-ground">
                              <div class="spinner-grow" role="status">
                                <span class="visually-hidden">Loading...</span>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-danger js-delete">Delete</button>
                          </div>
                        </div>
                      </div>`
    document.body.appendChild(this.modalElement)
    this.modal = new bootstrap.Modal(document.getElementById('modal'), {})
    this.modal.show();

    const removeBtn = this.modalElement.querySelector('.js-delete')
    removeBtn.addEventListener('click', this.removeAndClose.bind(this))
  }

  closeModal() {
    this.modal.dispose();
    this.modalElement.remove();
  }

}

class DeleteModal extends Modal {
  constructor(onRemove) {
    super()
    this.remove = onRemove;
  }

  async removeAndClose() {
    const loaderContainer = this.modalElement.querySelector('.js-loader-container')
    loaderContainer.classList.add('isShown')
    await this.remove()
    loaderContainer.classList.remove('isShown')
    this.closeModal()
  }

}
