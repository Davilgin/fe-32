class Student extends Entity {
  constructor(firstName, lastName, id) {
    super();
    this.firstName = firstName;
    this.lastName = lastName;
    this.element = null;
    this.id = id;
    this.listElement = document.querySelector(".js-student-list");
    this.collection = studentsCollection;
  }
}
