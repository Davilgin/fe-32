class TeachersCollection {
  constructor() {
    this.list = [];
  }

  addTeacher() {
    const teacherFirstName = document.querySelector(".js-teacher-first-name-input").value;
    const teacherLastName = document.querySelector(".js-teacher-last-name-input").value;

    const body = JSON.stringify({
      firstName: teacherFirstName,
      lastName: teacherLastName
    });

    fetchJSON('http://localhost:3000/teacher', {
      method: 'POST',
      headers: {
        'Content-type': 'application/json',
      },
      body,
    }).then((response) => {
      const newTeacher = response;
      const teacher = new Teacher(teacherFirstName, teacherLastName, newTeacher.id);
      this.list.push(teacher);
      teacher.render();
    });
  }

  filterTeachers(e) {
    const filteredList = this.list.filter((teacher) => { 
      const firstName = teacher.firstName; 
      return firstName.includes(e.target.value);
    });

    const listElement = document.querySelector('.js-teacher-list');
    listElement.innerHTML = '';

    filteredList.forEach(teacher => teacher.render());
  }

  getCollection() {
    fetchJSON('http://localhost:3000/teacher')
      .then((data) => {
        const collectionFromServer = data;
        collectionFromServer.forEach(element => {
          const firstName = element.firstName;
          const lastName = element.lastName;
          const id = element.id;
          const person = new Teacher(firstName, lastName, id);
          this.list.push(person);
          person.render();
        });
      });
  }

  removeById(id) {
    fetch('http://localhost:3000/teacher/${id}', {
      method: 'DELETE',
    }).then(() => {
      console.log(id);
      const teacherToRemove = this.list.find((teacher) => {
        return teacher.id === id;
      })
      teacherToRemove.element.remove();
     
      const teacherIndex = this.list.findIndex((person) => {
        return person.id === id;
      });
      this.list.splice(teacherIndex, 1);
    });
  }
}


const teachersCollection = new TeachersCollection();
teachersCollection.getCollection();

const addTeacherBtn = document.querySelector(".js-add-teacher-btn");
addTeacherBtn.addEventListener("click", teachersCollection.addTeacher.bind(teachersCollection));

const searchInput = document.querySelector('.js-teacher-search-input');
searchInput.addEventListener('input', teachersCollection.filterTeachers.bind(teachersCollection));