class StudentsCollection {
  constructor() {
    this.list = [];
  }
  async addStudent() {
    const studentFirstName = document.querySelector(".js-student-first-name-input").value;
    const studentLastName = document.querySelector(".js-student-last-name-input").value;
    const body = JSON.stringify({
      firstName: studentFirstName,
      lastName: studentLastName
    });
    const newStudent = await fetchJSON('http://localhost:3000/student', {
        method: 'POST',
        body,
        headers: {
          'Content-type': 'application/json'
        }
      });

    const { id } = newStudent;

    const responses = await Promise.all([this.setDefaultAge(id), this.setDefaultCountry(id)])

    const [response1, response2] = responses;
    const result = { ...response1, ...response2 };
    const student = new Student(studentFirstName, studentLastName, result.id);
    this.list.push(student);
    student.render();
  }
  setDefaultAge(id) {
    return fetchJSON(`http://localhost:3000/student/${id}`, {
          method: 'PATCH',
          body: JSON.stringify({
            age: 17
          }),
          headers: {
            'Content-type': 'application/json'
          }
        })
  }
  setDefaultCountry(id) {
    return fetchJSON(`http://localhost:3000/student/${id}`, {
          method: 'PATCH',
          body: JSON.stringify({
            country: 'UA'
          }),
          headers: {
            'Content-type': 'application/json'
          }
        })
  }
  async removeById(id) {
      await fetchJSON(`http://localhost:3000/student/${id}`, {
        method: 'DELETE',
      })
      const studentToRemove = this.list.find((student) => {
        return student.id === id;
      });
      studentToRemove.element.remove();

      const selfIndex = this.list.findIndex((person) => {
        return person.id === id;
      });
      this.list.splice(selfIndex, 1);
  }
  async getCollection() {
    const response = await fetchJSON('http://localhost:3000/student');

    const collectionFromServer = response;
    for (let element of collectionFromServer) {
      const firstName = element.firstName;
      const lastName = element.lastName;
      const id = element.id;
      const person = new Student(firstName, lastName, id);
      this.list.push(person);
      person.render();
      await wait(1000);
    }
  }
}

const studentsCollection = new StudentsCollection();
studentsCollection.getCollection();

const addStudentBtn = document.querySelector(".js-add-student-btn");
addStudentBtn.addEventListener("click", async function () {
  await studentsCollection.addStudent();
});