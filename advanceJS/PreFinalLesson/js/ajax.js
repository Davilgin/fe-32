const fetchJSON = (url, initRequest) =>
  fetch(url, initRequest)
    .then(response => response.json()); 