class Entity {
  setContent() {

    this.element.innerHTML = this.firstName + ' ' + this.lastName;

    const removeBtn = document.createElement("button");
    removeBtn.innerHTML = "Delete";
    removeBtn.classList.add('btn', 'btn-danger', 'm-1');
    this.element.appendChild(removeBtn);

    const editBtn = document.createElement("button");
    editBtn.innerHTML = "Edit";
    editBtn.classList.add('btn', 'btn-info', 'm-1');
    this.element.appendChild(editBtn);

    removeBtn.addEventListener("click", this.confirmRemove.bind(this));

    editBtn.addEventListener("click", this.startEdit.bind(this));
  }

  confirmRemove () {
    const deleteModal = new DeleteModal(this.remove.bind(this))
    deleteModal.openModal()
  }
  
  async render() {
    const listItem = document.createElement("li");
    
    this.listElement.appendChild(listItem);

    this.element = listItem;

    this.setContent();

    await wait(10);
    
    listItem.classList.add('isShown');  
  }

  async saveChanges() {
    const editFirstNameInput = this.element.querySelector('.js-edit-first-name');
    const editLastNameInput = this.element.querySelector('.js-edit-last-name');

    const response = await fetchJSON(`http://localhost:3000/student/${this.id}`, {
      method: 'PATCH',
      body: JSON.stringify({
        firstName: editFirstNameInput.value,
        lastName: editLastNameInput.value
      }),
      headers: {
        'Content-type': 'application/json'
      }
    });

    this.firstName = response.firstName;
    this.lastName = response.lastName;
    this.setContent();
  }

  startEdit() {
    this.element.innerHTML = `
      <input class="js-edit-first-name" value="${this.firstName}"/>
      <input class="js-edit-last-name" value="${this.lastName}"/>
      <button class="js-save-edit-btn">Save</button>`;

    const saveBtn = this.element.querySelector('.js-save-edit-btn');
    saveBtn.addEventListener('click', this.saveChanges.bind(this));
  }

  async remove() {
    console.log('Will wait');
    await wait(1000)
    console.log('Will remove');
    await this.collection.removeById(this.id)
  }

}
