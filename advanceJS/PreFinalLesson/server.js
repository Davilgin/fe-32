const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors')

const teacher1 = {
  id: 1,
  firstName: 'Sergey',
  lastName: 'Sergey'
}
const teacher2 = {
  id: 2,
  firstName: 'Nick',
  lastName: 'Nick',
}

const teachers = [teacher1, teacher2];

const students = [{
  firstName: 'Harry',
  lastName: 'Potter',
  id: 1,
}, {
  firstName: 'Vitya',
  lastName: 'Victor',
  id: 2,
}]

const app = express();
app.use(cors());
app.use(bodyParser.json());


app.delete('/teacher/:id', function (req, res) {
  const id = req.params.id;
  
  const index = teachers.findIndex((teacher) => {
    return teacher.id === Number(id);
  });
  const teacherToRemove = teachers[index];
  teachers.splice(index, 1);

  setTimeout(() => {
    res.send(teacherToRemove);
  }, 3000);
});

app.get('/teacher', function (req, res) {
  res.send(teachers);
});

app.post('/teacher', function (req, res) {
  const newTeacher = req.body;
  const personId = teachers.length + 1; 

  const teacherWithId = { 
    ...newTeacher,
    id: personId 
  };

  teachers.push(teacherWithId);
  res.send(teacherWithId);
});

app.delete('/student/:id', function (req, res) {
  const id = req.params.id;
  
  const index = students.findIndex((student) => {
    return student.id === Number(id);
  });
  const studentToRemove = students[index];
  students.splice(index, 1);

  setTimeout(() => {
    res.send(studentToRemove);
  }, 3000);
});

app.post('/student', function (req, res) {
  const newStudent = req.body;
  const personId = students.length + 1;

  const studentWithId = { 
    ...newStudent,
    id: personId 
  };

  students.push(studentWithId);
  res.send(studentWithId);
});


app.patch('/student/:id', function (req, res) {
  const student = req.body;
  const id = req.params.id;

  const oldStudentIndex = students.findIndex((student) => student.id === Number(id));
  const oldStudent = students[oldStudentIndex];

  const newStudent = { ...oldStudent, ...student };
  students[oldStudentIndex] = newStudent;
  setTimeout(() => {
    res.send(newStudent);
  }, 2000);
  
});

app.get('/student', function (req, res) {
  res.send(students);
});

app.listen(3000, function () {
  console.log('Listening on 3000');
});