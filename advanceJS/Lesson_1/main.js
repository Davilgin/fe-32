const groupList = [];

function Group(name, id) {
    this.name = name;
    this.element = null;
    this.id = id;
    this.deleteItem = () => this.element.remove();

    this.render = function () {
        const list = document.querySelector(".js-group-list");
        const listItem = document.createElement('li');
        const removeBtn = document.createElement("button");

        removeBtn.innerHTML = "X";
        listItem.innerHTML = this.name;

        list.appendChild(listItem);
        this.element = listItem;
        listItem.appendChild(removeBtn);

        removeBtn.addEventListener("click", this.deleteItem);
        // console.log(this.element);
        // console.log(this);
    }
}

const addGroupBtn = document.querySelector(".js-add-group-btn");
addGroupBtn.addEventListener("click", function () {
    const groupId = groupList.length + 1;
    const groupName = document.querySelector(".js-add-group-name-input").value;
    const group = new Group(groupName, groupId);
    groupList.push(group);

    group.render();
    // console.log(group);
    // console.log(Group);
});

// function wait(callback) {
//     setTimeout(function () {
//         callback();
//     }, 1000);
// }