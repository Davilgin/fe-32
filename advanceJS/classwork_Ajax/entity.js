class Entity {
  render() {
    const listItem = document.createElement("li");
    listItem.innerHTML = this.firstName + ' ' + this.lastName;
    this.listElement.appendChild(listItem);
    this.element = listItem;

    const removeBtn = document.createElement("button");
    removeBtn.innerHTML = "Delete";
    listItem.appendChild(removeBtn);

    removeBtn.addEventListener("click", this.remove.bind(this));
  }

  remove() {
    this.element.remove();
    this.collection.removeById(this.id);
  }
}
