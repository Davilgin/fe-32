const teacherList = [];

class TeachersCollection extends BaseCollection {
  constructor() {
    super();
    this.list = teacherList;
    this.firstNameElement = document.querySelector(".js-teacher-first-name-input");
    this.lastNameElement = document.querySelector(".js-teacher-last-name-input");
    this.PersonConstructor = Teacher;
    this.localStorageKey = 'teachersCollection';
  }

  filterTeachers(e) {
    const filteredList = teacherList.filter((teacher) => { 
      const firstName = teacher.firstName; 
      return firstName.includes(e.target.value);
    });
    console.log(filteredList);

    const listElement = document.querySelector('.js-teacher-list');
    listElement.innerHTML = '';

    filteredList.forEach(teacher => teacher.render());
  }

  getCollection() {
    try {
      const oReq = new XMLHttpRequest();
      oReq.addEventListener("load", () => {
        const collectionFromServer = JSON.parse(oReq.responseText);
        collectionFromServer.forEach(element => {
          const firstName = element.firstName;
          const lastName = element.lastName;
          const id = element.id;
          const person = new Teacher(firstName, lastName, id);
          this.list.push(person);
          person.render();
        });
      });

      oReq.open("GET", "http://localhost:3000/teacher");
      oReq.send();
    } catch (error) {
      console.log(error);
    }
  }
}


const teachersCollection = new TeachersCollection();
teachersCollection.getCollection();

const addTeacherBtn = document.querySelector(".js-add-teacher-btn");
addTeacherBtn.addEventListener("click", teachersCollection.addPerson.bind(teachersCollection));

const searchInput = document.querySelector('.js-teacher-search-input');
searchInput.addEventListener('input', teachersCollection.filterTeachers);