const express = require('express');
const cors = require('cors');

const teacher1 = ({
    id: 1,
    firstName: "Serj",
    lastName: "Serj"
});

const teacher2 = ({
    id: 1,
    firstName: "Nick",
    lastName: "Name"
});

const student1 = ({
    id: 1,
    firstName: "Serj",
    lastName: "Serj"
});

const student2 = ({
    id: 1,
    firstName: "Nick",
    lastName: "Name"
});

const teachers = [teacher1, teacher2];
const students = [student1, student2];

const app = express();
app.use(cors());

app.get('/teacher', function (req, res) {
    res.send(teachers);
});

app.delete('/teacher/:id', function (req, res) {
    console.log(req.params);
    res.send(teachers);
});

app.get('/student', function (req, res) {
    res.send(students);
});

app.listen(3000, function () {
    console.log('Listening on 3000');
});
