const studentList = [];

function StudentsCollection () {
  this.list = studentList;
  this.addStudent = function () {
    const studentId = studentList.length + 1;
    const studentFirstName = document.querySelector(".js-student-first-name-input").value;
    const studentLastName = document.querySelector(".js-student-last-name-input").value;
    const student = new Student(studentFirstName, studentLastName, studentId);
    studentList.push(student);
    student.render();
    this.syncCollection();
  };
  this.syncCollection = function () {
    const studentListToSave = studentList.map((student) => {
      const firstName = student.firstName;
      const lastName = student.lastName;
      const id = student.id;

      return { firstName, lastName, id };
    });

    localStorage.setItem('studentsCollection', JSON.stringify({ 
      studentList: studentListToSave
    }));
  };
  this.getCollection = function() {
    try {
      const oReq = new XMLHttpRequest();
      oReq.addEventListener("load", () => {
        const collectionFromServer = JSON.parse(oReq.responseText);
        collectionFromServer.forEach(element => {
          const firstName = element.firstName;
          const lastName = element.lastName;
          const id = element.id;
          const person = new Student(firstName, lastName, id);
          this.list.push(person);
          person.render();
        });
      });

      oReq.open("GET", "http://localhost:3000/students");
      oReq.send();
    } catch (error) {
      console.log(error);
    }
  };
  this.removeById = function (id) {
    const selfIndex = studentList.findIndex((person) => {
      return person.id === id;
    });
    studentList.splice(selfIndex, 1);
    this.syncCollection();
  }
}

const studentsCollection = new StudentsCollection();
studentsCollection.getCollection();

const addStudentBtn = document.querySelector(".js-add-student-btn");
addStudentBtn.addEventListener("click", studentsCollection.addStudent.bind(studentsCollection));