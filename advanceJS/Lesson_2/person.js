function Entity() {
    this.deleteItem = function() {
        this.element.remove();
        const selfIndex = this.list.findIndex((person) => {
            return person.id === this.id;
        });

        this.list.splice(selfIndex, 1);
    };

    this.render = function () {
        // const listElement = document.querySelector(".js-student-list");
        const listItem = document.createElement('li');
        const removeBtn = document.createElement("button");

        removeBtn.innerHTML = "X";
        listItem.innerHTML = this.firstName + ' ' + this.lastName;

        this.listElement.appendChild(listItem);
        this.element = listItem;
        listItem.appendChild(removeBtn);

        removeBtn.addEventListener("click", this.deleteItem.bind(this));
        // console.log(this.element);
        // console.log(this);
    }
}