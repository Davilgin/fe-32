const studentList = [];

function Student(firstName, lastName, id) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.element = null;
    this.list = studentList;
    this.id = id;
    this.listElement = document.querySelector(".js-student-list");
    // this.deleteItem = () => {
    //     this.element.remove();
    //     const selfIndex = studentList.findIndex((student) => {
    //         return student.id === this.id;
    //     });
    //
    //     studentList.splice(selfIndex, 1);
    //     console.log(studentList)
    // };
}

const addStudentBtn = document.querySelector(".js-add-student-btn");
addStudentBtn.addEventListener("click", function () {
    const studentId = studentList.length + 1;
    const studentFirstName = document.querySelector(".js-student-first-name-input").value;
    const studentLastName = document.querySelector(".js-student-last-name-input").value;
    const student = new Student(studentFirstName, studentLastName, studentId);
    studentList.push(student);

    student.render();
});

Student.prototype = new Entity();