const teacherList = [];

function Teacher(firstName, lastName, id) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.element = null;
    this.list = teacherList;
    this.id = id;
    this.listElement = document.querySelector(".js-teacher-list");

}

const addTeacherBtn = document.querySelector(".js-add-teacher-btn");
addTeacherBtn.addEventListener("click", function () {
    const studentId = teacherList.length + 1;
    const teacherFirstName = document.querySelector(".js-teacher-first-name-input").value;
    const teacherLastName = document.querySelector(".js-teacher-last-name-input").value;
    const teacher = new Teacher(teacherFirstName, teacherLastName, studentId);
    teacherList.push(teacher);

    teacher.render();
});

Teacher.prototype = new Entity();