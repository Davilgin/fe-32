const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const list = document.createElement('ul');
const root = document.querySelector('#root');
root.appendChild(list);

let wrongObj = [];

books.forEach(function (e) {

    const {author, name, price} = e;

    if (!(author === undefined || name === undefined || price === undefined)) {
        let item = document.createElement('li');
        item.textContent = `Author: ${author} | book name: ${name} | price: ${price}`;
        list.appendChild(item);

    } else {
        try {
            let arr = ['author', 'name', 'price'];
            wrongObj.push(e);
            arr.forEach(prop => {
                if (!(e.hasOwnProperty(prop))) {
                    console.log(e, `Error in: ${prop}`);
                    throw new Error(`${prop} is missed in object`);
                }
            });
        } catch (error) {
            console.log(error);
        }
    }

});

console.log(wrongObj);

