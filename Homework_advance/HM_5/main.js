const usersUrl = "https://ajax.test-danit.com/api/json/users";
const postsUrl = "https://ajax.test-danit.com/api/json/posts";
let postId = 100;

class Card {
    constructor(postId, title, body, fullName, email) {
        this.postId = postId;
        this.title = title;
        this.body = body;
        this.fullName = fullName;
        this.email = email;
    }

    render() {
        this.posts = document.querySelector(".posts");
        this.post = document.querySelector(".post").cloneNode(true);
        this.posts.prepend(this.post);
        this.post.classList.add('js-active');
        this.setContent();
    }

    setContent() {
        this.postTitle = this.post.querySelector(".js-title");
        this.postTitle.innerHTML = this.title;
        this.postTitle.classList.add("post-title");
        this.text = this.post.querySelector(".js-text");
        this.text.innerHTML = this.body;
        this.authorData = this.post.querySelector('.author-data');
        this.post.querySelector(".js-author-name").innerHTML = this.fullName;
        this.post.querySelector(".js-email").innerHTML = this.email;
        this.post.querySelector(".js-email").href = `mailto:${this.email}`;

        this.post.querySelector(".delete-btn").addEventListener("click", this.delete.bind(this));

        this.post.querySelector(".edit-btn").addEventListener('click', this.edit.bind(this))
    }

    delete() {
        fetchJSON(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
            method: "DELETE"
        }).then(this.post.remove());
    }

    edit() {
        this.postTitle.innerHTML = `<input class="js-edit-title" name="edit-title"><label for="edit-title">Enter new title</label>`;
        this.postTitle.classList.remove("post-title");
        this.text.innerHTML = `<input class="js-edit-text"name="edit-text"><label for="edit-text">Enter new message</label>`;

        if (!this.post.querySelector('.save-btn')) {
            this.saveBtn = document.createElement("button");
        }
        this.saveBtn.innerHTML = 'Save changes <i class="far fa-save"></i>';
        this.saveBtn.classList.add('save-btn');
        this.post.insertBefore(this.saveBtn, this.authorData);

        this.saveBtn.addEventListener("click", this.saveChanges.bind(this));
    }

    saveChanges() {
        this.title = document.querySelector(".js-edit-title").value;
        this.body = document.querySelector(".js-edit-text").value;
        fetchJSON(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
            method: "PUT",
            body: JSON.stringify({
                title: this.title,
                body: this.body,
            }),
            headers: {
                'Content-type': 'application/json'
            }
        }).then(this.setContent());
        this.saveBtn.remove();
    }
}

const fetchJSON = (url, initRequest) =>
    fetch(url, initRequest)
        .then(response => response.json());

function loadCards() {
    Promise.all([fetchJSON(postsUrl), fetchJSON(usersUrl)]).then(([posts, users]) => {
        posts.reverse().forEach(({
                                     id: postId,
                                     userId,
                                     title,
                                     body
                                 }) => {
            users.reverse().forEach(({
                                         id,
                                         name: fullName,
                                         email
                                     }) => {
                if (id === userId) {
                    const card = new Card(postId, title, body, fullName, email)
                    card.render();
                }
            })
        })
    })
}

function showAndCloseInput() {
    const addPostBtn = document.querySelector(".add-post-btn");
    const sendForm = document.querySelector(".input-popup");

    addPostBtn.addEventListener("click", function (event) {
        sendForm.classList.toggle("js-active-form");
        event.stopPropagation();
    })

    document.addEventListener("click", function (event) {
        if (event.target != sendForm && event.target.closest("form") != sendForm) {
            if (sendForm.classList.contains("js-active-form")) {
                sendForm.classList.remove("js-active-form");
            }
        }
    })
}

function addNewPost() {
    const publicBtn = document.querySelector(".public-btn");

    publicBtn.addEventListener("click", function (event) {
        event.preventDefault();
        postId++;
        const newCard = new Card(
            postId,
            document.querySelector(".input-title").value,
            document.querySelector(".input-message").value,
            "Leanne Graham",
            "Sincere@april.biz"
        );
        fetchJSON(postsUrl, {
            method: "POST",
            body: JSON.stringify(newCard),
            headers: {
                'Content-type': 'application/json'
            }
        }).then(newCard.render());
    })
}

loadCards();
showAndCloseInput();
addNewPost();
