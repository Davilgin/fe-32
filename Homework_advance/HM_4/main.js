const requestUrl = 'https://ajax.test-danit.com/api/swapi/films';

const container = document.querySelector('.table');

fetch(requestUrl)
    .then((response) => {
        return response.json();
    })
    .then((data) => {
        data.forEach(({name, episodeId, openingCrawl, characters}) => {
            const movie = new Serial(name, episodeId, openingCrawl, characters);
            movie.render();
        });
    });

class Serial {
    constructor(name, episodeId, openingCrawl, characters) {
        this.name = name;
        this.episodeId = episodeId;
        this.openingCrawl = openingCrawl;
        this.characters = characters;
    }

    render() {
        const filmWrapper = document.createElement('div');
        filmWrapper.innerHTML =
            `<h2>STAR WARS</h2>
             <h3>Episode: ${this.episodeId} <br> ${this.name}</h3>
             <p>${this.openingCrawl}</p>`;
        container.appendChild(filmWrapper);

        const characterList = document.createElement('ul');
        characterList.innerHTML = 'Characters :';

        this.characters.forEach(character => {
            fetch(character)
                .then((response) => {
                    return response.json();
                })
                .then((data) => {
                    filmWrapper.appendChild(characterList);
                    const listItem = document.createElement('li');
                    characterList.appendChild(listItem);
                    listItem.innerHTML = data.name;
                })
        })
    }
}
