class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get name() {
        return this._name;
    }

    get age() {
        return this._age;
    }

    get salary() {
        return this._salary;
    }

    set name(name) {
        if (name ===" " || !isNaN(name) || name === undefined){
            console.log(`${name} is not name`);
            return
        }

        this._name = name;
        return this._name;
    }

    set age(years) {
        if (years === " " || isNaN(years) || years === undefined){
            console.log(`${years} is not age`);
            return
        }
        this._age = years;
        return this._age;
    }

    set salary(value) {
        if (value === " " || isNaN(value) || value === undefined){
            console.log(`${value} is not salary`);
            return
        }

        this._salary = value;
        return this._salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang = []) {
        super(name, age, salary);
        this.lang = lang;
    }

    set salary(value) {
        // if need some checkout;
        this._salary = value * 3;
        return this._salary;
    }

    get lang() {
        return this._lang;
    }

    set lang(lang) {
        // if need some checkout;
        this._lang = lang;
        return this._lang;
    }

}

const frontend = new Programmer('Anna', 36, 11000, ['JS']);
const backend = new Programmer('Mariia', 21, 24000, ['JS', 'PHP']);
const fullstack = new Programmer('Davyd', 23, 28000, ['JS', 'PHP', 'Ruby']);

console.log(frontend);
console.log(backend);
console.log(fullstack);
