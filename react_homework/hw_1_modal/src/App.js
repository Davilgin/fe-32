import React from 'react';
import './App.scss';
import Button from './components/Button/Button.jsx';
import Modal from './components/Modal/Modal.jsx';
import modalTypes from './components/Modal';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      modalToOpen: "none",
      modalContent: {}
    }
  }

  openModal = (event) => {
    const modal = modalTypes.find(item => {
      return item.id === event.target.dataset.id;
    });

    this.setState({
      modalToOpen: modal.id,
      modalContent: modal
    })
  };

  closeModal = () => {
    this.setState({
      modalToOpen: "none",
      modalContent: {}
    })
  };

  render(){
    return (
        <>
          <Button id ="1" text="Open first modal" color="red" func={this.openModal} />
          <Button id ="2" text="Open second modal" color="green" func={this.openModal} />
          {this.state.modalToOpen !== "none" && <>
            <div onClick={this.closeModal} className="backView"></div>
            <Modal header={this.state.modalContent.title}
                   closeButton={this.state.modalContent.closeButton}
                   text={this.state.modalContent.text}
                   color={this.state.modalContent.color}
                   closeModal={this.closeModal}
                   actions={<div className="modal-btns">
                     <Button text="Yes!" color="black"></Button>
                     <Button text="Not sure..." color="silver"></Button>
                   </div>}
            />
          </>}
        </>
    );
  }

}

export default App;
