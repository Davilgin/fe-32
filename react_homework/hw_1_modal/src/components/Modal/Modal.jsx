import React from 'react';
import "./Modal.scss";

class Modal extends React.Component {
    render() {
        const { header, closeButton, text, color, closeModal, actions } = this.props;
        return (
            <div className="modal" style={{ backgroundColor: color }}>
                <div className="modal-header">
                    <h2 className="title">{header}</h2>
                    {closeButton && <i onClick={closeModal} class="fas fa-times"></i>}
                </div>
                <p className="text">{text}</p>
                {actions}
            </div>
        )
    }
}

export default Modal;
