const modalTypes = [
    {
        id: "1",
        title: "Here is RED modal",
        text: "Now you saw the Red modal window, right?",
        color: "red",
        closeButton: true
    },
    {
        id: "2",
        title: "Here is GREEN modal",
        text: "Now you saw colour of Nature, as now your day will roll better?",
        color: "green",
        closeButton: false
    }
];

export default modalTypes;
