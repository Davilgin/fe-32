import React from 'react';
import "./Button.scss";

class Button extends React.Component {
    render() {
        const { color, text, func, id } = this.props;
        return (
            <a data-id={id} className="button" onClick={func} style={{ backgroundColor: color }}>{text}</a>
        )
    }
}

export default Button;
