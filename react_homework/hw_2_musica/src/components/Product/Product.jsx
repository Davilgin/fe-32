import React from 'react';
import Card from '../Card/Card';
import Button from '../Button/Button';
import Modal from '../Modal/Modal';

class ProductList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            productsToShow: [],
            productsToCart: [],
            favouriteProducts: localStorage.getItem('favorites') || [],
            cardToCart: '',
        }
    }

    openModal = (id) => {
        this.setState({
            cardToCart: id
        })
    }

    closeModal = () => {
        this.setState({
            cardToCart: ''
        })
    }

    addToCart = () => {
        if (!this.state.productsToCart.includes(this.state.cardToCart)) {
            this.setState({
                productsToCart: [...this.state.productsToCart, this.state.cardToCart]
            })
        }

        this.closeModal();
    }

    addToFavourites = (id) => {
        this.setState({
            favouriteProducts: [...this.state.favouriteProducts, id]
        })
    }

    removeFromFavourites = (id) => {
        const newFavouriteList = this.state.favouriteProducts.filter(product => {
            return product !== id;
        });

        this.setState({
            favouriteProducts: newFavouriteList
        });
    }

    componentDidMount() {
        if (typeof (this.state.favouriteProducts) === 'string') {
            this.setState({
                favouriteProducts: this.state.favouriteProducts.split(',')
            })
        }

        fetch('./products.json')
            .then(response => response.json())
            .then(products => {
                this.setState({
                    productsToShow: products
                })
            }).catch((err) => {
            console.log(`'Error was found:' ${err}`);
        })
    }

    render() {
        {
            this.state.productsToCart.length > 0 && localStorage.setItem('cart', this.state.productsToCart)
        }

        {
            this.state.favouriteProducts.length > 0 ? localStorage.setItem('favourites', this.state.favouriteProducts) : localStorage.removeItem('favourites')
        }

        return (
            <>
                {
                    this.state.productsToShow.map(product => {
                        return <Card key={product.id} id={product.id} image={product.img} name={product.name}
                                     color={product.color} code={product.code} price={product.price}
                                     openModal={this.openModal} addToFavourites={this.addToFavourites}
                                     removeFromFavourites={this.removeFromFavourites}
                                     favouriteProducts={this.state.favouriteProducts}/>
                    })
                }

                {
                    this.state.cardToCart !== '' && <>
                        <div onClick={this.closeModal} className='backView'></div>
                        <Modal header='Add this?'
                               text='Click "Ok" to agree'
                               color='blue'
                               closeModal={this.closeModal}
                               actions=
                                   {<div className='modal-btns'>
                                       <Button text='Ok' func={this.addToCart} color='black'></Button>
                                       <Button text='Cancel' func={this.closeModal} color='grey'></Button>
                                   </div>}
                        />
                    </>
                }
            </>
        )
    }
}

export default ProductList;