import React from 'react';
import PropTypes from 'prop-types';
import "./Button.scss";

class Button extends React.Component {
    render() {
        const { color, text, func, id } = this.props;
        return (
            <a data-id={id} className="button" onClick={func} style={{ backgroundColor: color }}>{text}</a>
        )
    }
}

Button.propTypes = {
    func: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired,
    color: PropTypes.string,
};

Button.defaultProps = {
    color: 'grey'
};

export default Button;
