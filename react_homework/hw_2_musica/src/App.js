import React from 'react';
import './App.scss';
import ProductList from "./components/Product/Product";

class App extends React.Component {
  render(){
    return (
        <ProductList />
    );
  }
}

export default App;
